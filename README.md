# wso2mi-hl7-demo-docker

## How to run 

```
docker run -it --rm -p 8280:8280  -p 9292:9292 -p 8290:8290 -p 20000:20000 -p 9393:9393 -p 9494:9494 -p 9595:9595 -e HL7_BE_URL=hl7://host.docker.internal:9988  registry.gitlab.com/sagaragu.dev/wso2-mi-demo:latest
```


## How to test

[How to test the samples](https://github.com/sagara-gunathunga/hl7-wso2-integration-samples/blob/master/README.md#how-to-test)